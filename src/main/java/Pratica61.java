
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time t1 = new Time();
        Jogador j11 = new Jogador(1,"Fulano");Jogador j12 = new Jogador(4,"Ciclano");
        Jogador j13 = new Jogador(10,"Beltrano");
        t1.addJogadores("Goleiro", j11);t1.addJogadores("Lateral", j12);
        t1.addJogadores("Atacant", j13);
        
        Time t2 = new Time();
        Jogador j21 = new Jogador(1,"João");Jogador j22 = new Jogador(7,"José");
        Jogador j23 = new Jogador(15,"Mário");
        t2.addJogadores("Atacant", j23);
        t2.addJogadores("Goleiro", j21);t2.addJogadores("Lateral", j22);
        
        System.out.println("Posição      Time1        Time2");
        for (String key1 : t1.getJogadores().keySet()) {
            //Capturamos o valor a partir da chave
            Jogador value1 = t1.getJogadores().get(key1);
            for (String key2 : t2.getJogadores().keySet()){
                Jogador value2 = t2.getJogadores().get(key2);
                if (key1==key2){
                    System.out.println(key1 + "\t" + value1.getNumero() + " - " + value1.getNome()+ "\t" + value2.getNumero() + " - " + value2.getNome());
                }
            }
        }
    }
}